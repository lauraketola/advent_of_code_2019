defmodule AdventOfCode do
  @moduledoc """
  Provides `AdventOfCode.main/1` to print solutions for Advent of Code 2019.
  """

  defp print_solution({day, part, fun}) do
    day = Integer.to_string(day)
    filename = "test/data/day" <> day
    IO.puts("Day #{day}, part #{part}: #{fun.(filename)}")
  end

  @doc "Prints all implemented solutions."
  def main(_args \\ []) do
    Enum.each(
      [
        {1, 1, &Day1.solution_part1/1},
        {1, 2, &Day1.solution_part2/1},
        {2, 1, &Day2.solution_part1/1},
        {2, 2, &Day2.solution_part2/1},
        {3, 1, &Day3.solution_part1/1},
        {3, 2, &Day3.solution_part2/1},
        {4, 1, &Day4.solution_part1/1},
        {4, 2, &Day4.solution_part2/1},
        {5, 1, &Day5.solution_part1/1},
        {5, 2, &Day5.solution_part2/1},
        {6, 1, &Day6.solution_part1/1},
        {6, 2, &Day6.solution_part2/1},
        {7, 1, &Day6.solution_part1/1},
        {7, 2, &Day6.solution_part2/1}
      ],
      &print_solution/1
    )
  end
end
