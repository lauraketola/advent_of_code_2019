defmodule Day4Test do
  use ExUnit.Case, async: true

  test "day 4, part 1 hints" do
    assert Day4.parse_string("12") == [1, 2]
    assert Day4.check_string("111111")
    assert Day4.check_string("111123")
    refute Day4.check_string("223450")
    refute Day4.check_string("123789")
  end

  test "day 4, part 1 solution" do
    assert Day4.solution_part1("test/data/day4") == 475
  end

  test "day 4, part 2" do
    assert Day4.check_string_no_run("112233")
    refute Day4.check_string_no_run("123444")
    assert Day4.check_string_no_run("111122")
    assert Day4.check_string_no_run("111233")

    refute Day4.check_string_no_run("111111")
    refute Day4.check_string_no_run("111123")
    refute Day4.check_string_no_run("223450")
    refute Day4.check_string_no_run("123789")

    assert Day4.check_string_no_run("111122")
    assert Day4.check_string_no_run("112222")
    assert Day4.check_string_no_run("122334")
    refute Day4.check_string_no_run("122224")
    refute Day4.check_string_no_run("122224")
    refute Day4.check_string_no_run("111112")
    refute Day4.check_string_no_run("122222")
  end

  test "day4, part 2 solution" do
    assert Day4.solution_part2("test/data/day4") == 297
  end
end
