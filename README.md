# Advent of Code

Solutions for Advent of Code 2019 in [Elixir](https://elixir-lang.org).

- `AdventOfCode.main/1` prints the solutions
- `Intcode` interpreter as separate module
- Unit tested, run with `mix test`
- Build the documentation with `mix docs`
