defmodule Day2Test do
  use ExUnit.Case, async: true

  test "day 2, part 1" do
    assert Day2.solution_part1("test/data/day2") == 3_516_593
  end

  test "day 2, part 2" do
    assert Day2.solution_part2("test/data/day2") == 7749
  end
end
