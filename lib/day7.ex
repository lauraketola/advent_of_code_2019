defmodule Day7 do
  @moduledoc "Amplification Circuit"

  def solution_part1(filename) do
    program = Intcode.loadinput(filename)

    Enum.map(get_permutations(), fn permutation ->
      run(program, permutation)
    end)
    |> Enum.max()
  end

  def solution_part2(filename) do
    program = Intcode.loadinput(filename)
    permutations = permute([5, 6, 7, 8, 9])

    Enum.each(permutations, fn permutation ->
      feedback_loop(program, permutation)
    end)

    # Receive all results and select maximum value
    Enum.map(0..(length(permutations) - 1), fn _ ->
      receive do
        {:intcode_out, {_, [channel: _, out: out]}} -> hd(out)
      end
    end)
    |> Enum.max()
  end

  @doc "Run the program once for every setting. Returns the output of the final program."
  def run(program, phase_settings) do
    Enum.reduce(phase_settings, 0, fn x, prev_output ->
      out = Intcode.run(program, [x, prev_output])
      hd(out)
    end)
  end

  @doc "Start the program once for every setting, setting up a loop."
  def feedback_loop(program, phase_settings) do
    # Start the amplifiers in seperate processes
    processes =
      Enum.map(0..4, fn index ->
        final = if index == 4, do: self(), else: nil
        Intcode.run_process(program, final)
      end)

    Enum.each(0..4, fn index ->
      process = Enum.at(processes, index)
      next = Enum.at(processes, index + 1, Enum.at(processes, 0))

      # Send the PID of the next amplifier in line
      send(process, {:intcode_pid, next})

      # Send the phase setting
      send(process, {:intcode_msg, Enum.at(phase_settings, index)})
    end)

    # Send initial input to the first amplifier
    send(hd(processes), {:intcode_msg, 0})
  end

  defp get_permutations do
    permute([0, 1, 2, 3, 4])
  end

  defp permute([]), do: [[]]

  defp permute(list) do
    for x <- list, y <- permute(list -- [x]), do: [x | y]
  end
end
