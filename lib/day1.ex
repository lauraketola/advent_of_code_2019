defmodule Day1 do
  @moduledoc "The Tyranny of the Rocket Equation"

  def solution_part1(filename) do
    solve(&fuel1/1, filename)
  end

  def solution_part2(filename) do
    solve(&fuel2/1, filename)
  end

  defp fuel1(m) do
    div(m, 3) - 2
  end

  defp fuel2(m) when m > 6 do
    f = div(m, 3) - 2
    fuel2(f) + f
  end

  defp fuel2(_) do
    0
  end

  defp solve(calc_fuel, filename) do
    File.read!(filename)
    |> String.split("\n")
    |> List.delete("")
    |> Enum.map(&String.to_integer/1)
    |> Enum.reduce(0, &(calc_fuel.(&1) + &2))
  end
end
