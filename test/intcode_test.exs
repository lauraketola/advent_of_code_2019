ExUnit.start()

defmodule IntcodeTest do
  use ExUnit.Case, async: true

  test "intcode, day 2" do
    assert Intcode.execute([1, 0, 0, 0, 99]) |> Intcode.memdump() ==
             "2,0,0,0,99"

    assert Intcode.execute([2, 3, 0, 3, 99]) |> Intcode.memdump() ==
             "2,3,0,6,99"

    assert Intcode.execute([2, 4, 4, 5, 99, 0]) |> Intcode.memdump() ==
             "2,4,4,5,99,9801"

    assert Intcode.execute([1, 1, 1, 4, 99, 5, 6, 0, 99]) |> Intcode.memdump() ==
             "30,1,1,4,2,5,6,0,99"
  end
end
