defmodule Day5 do
  @moduledoc "Sunny with a Chance of Asteroids"

  def solution_part1(filename) do
    input = Intcode.loadinput(filename)
    hd(Intcode.run(input, [1]))
  end

  def solution_part2(filename) do
    input = Intcode.loadinput(filename)
    hd(Intcode.run(input, [5]))
  end
end
