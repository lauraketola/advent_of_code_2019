defmodule Intcode do
  @moduledoc """
  Intcode interpreter.

  ## Position and immediate mode

  Two modes for instruction arguments are available. Position mode reads from the address specified, immediate mode uses the value itself. From right-to-left, two digits are for opcode, and the rest of the digits define mode for argument n.

  - Position mode opcode:  00001 / 1
  - Immediate mode opcode: 11101 (as many flags as there are parameters)

  ## Implemented instructions

  operator        | opcode | params
  --------------- | ------ | --------------
  add             | 1      | a, b, result
  multiply        | 2      | a, b, result
  read input      | 3      | write address
  write output    | 4      | read address
  jump-if-true    | 5      | bool, pointer
  jump-if-false   | 6      | bool, pointer
  less than       | 7      | a, b, result
  equals          | 8      | a, b, result
  halt            | 99     |

  """

  @doc """
  Run a program that reads process messages as input. 

  Requires `:intcode_pid` message with PID to write output to.

  If `result_pid` is given, sends the VM state to that process when finished.

  message      | direction  | value
  ------------ | ---------- | -----------------------------------------------
  :intcode_out | OUT        | VM state 
  :intcode_pid | IN         | PID to send output to when using "write output"
  :intcode_msg | OUT        | Message when using "write output"

  """
  def run_process(program, result_pid) do
    spawn(fn ->
      output_pid =
        receive do
          {:intcode_pid, pid} -> pid
        end

      result = execute(0, program, channel: output_pid, out: [])

      if is_pid(result_pid) do
        send(result_pid, {:intcode_out, result})
      end
    end)
  end

  @doc """
  Run a program with input list and return the output list.
  """
  def run(program, input) do
    {_, io} = execute(0, program, in: input, out: [])
    io[:out]
  end

  @doc """
  Run a program and return the value of address 0.
  """
  def run(program) do
    {memory, _} = execute(0, program, [])
    Enum.at(memory, 0)
  end

  @doc """
  Run a program with parameters i and j and return the value of address 0.
  """
  def run(program, i, j) do
    memory =
      program
      |> List.replace_at(1, i)
      |> List.replace_at(2, j)

    run(memory)
  end

  @doc """
  Execute a given program and return memory.
  """
  def execute(program) do
    {memory, _} = execute(0, program, [])
    memory
  end

  @doc """
  Execute a given program and return memory and output.
  """
  def execute(program, input) do
    execute(0, program, in: input, out: [])
  end

  @doc """
  Print out memory.
  """
  def memdump(memory) do
    memory
    |> Enum.map(&Integer.to_string/1)
    |> Enum.reduce("", &(&2 <> "," <> &1))
    |> String.slice(1..-1)
  end

  @doc """
  Loads a comma separated program from a given path.
  """
  def loadinput(file) do
    File.read!(file)
    |> String.trim()
    |> String.split(",")
    |> Enum.map(&String.to_integer/1)
  end

  # Private methods

  defp execute(pointer, memory, io) do
    if pointer < length(memory) do
      {new_pointer, new_memory, new_io} = parse_instruction(memory, pointer, io)
      execute(new_pointer, new_memory, new_io)
    else
      {memory, io}
    end
  end

  defp parse_instruction(memory, pointer, io) do
    digits =
      Enum.at(memory, pointer)
      |> Integer.digits()
      |> Enum.reverse()

    opcode_list = digits |> Enum.slice(0, 2)
    opcode = Enum.at(opcode_list, 0) + Enum.at(opcode_list, 1, 0) * 10

    modes = digits |> Enum.slice(2..-1)

    case opcode do
      # add
      1 ->
        {pointer + 4,
         change(
           memory,
           pointer + 3,
           reference(memory, pointer, modes, 1) +
             reference(memory, pointer, modes, 2)
         ), io}

      # multiply
      2 ->
        {pointer + 4,
         change(
           memory,
           pointer + 3,
           reference(memory, pointer, modes, 1) *
             reference(memory, pointer, modes, 2)
         ), io}

      # read input
      3 ->
        # Read from io[:in] Enumerable, if available,
        # otherwise expect a message
        stdin = Keyword.get(io, :in, nil)
        stdout = Keyword.get(io, :out, nil)

        {read_value, new_io} =
          if stdin == nil do
            receive do
              {:intcode_msg, value} -> {value, io}
            end
          else
            {Enum.at(stdin, 0, -999),
             [in: Enum.slice(stdin, 1..-1), out: stdout]}
          end

        {pointer + 2, change(memory, pointer + 1, read_value), new_io}

      # write output
      4 ->
        # Write to io[:out] list, if available,
        # otherwise send a message
        stdin = Keyword.get(io, :in, nil)
        stdout = Keyword.get(io, :out, nil)
        value = reference(memory, pointer, modes, 1)

        new_io =
          if stdin == nil do
            send(io[:channel], {:intcode_msg, value})
            [channel: io[:channel], out: [value | stdout]]
          else
            [in: stdin, out: [value | stdout]]
          end

        {pointer + 2, memory, new_io}

      # jump-if-true
      5 ->
        new_pointer =
          if reference(memory, pointer, modes, 1) != 0 do
            reference(memory, pointer, modes, 2)
          else
            pointer + 3
          end

        {new_pointer, memory, io}

      # jump-if-false
      6 ->
        new_pointer =
          if reference(memory, pointer, modes, 1) == 0 do
            reference(memory, pointer, modes, 2)
          else
            pointer + 3
          end

        {new_pointer, memory, io}

      # less than
      7 ->
        a = reference(memory, pointer, modes, 1)
        b = reference(memory, pointer, modes, 2)

        value =
          if a < b do
            1
          else
            0
          end

        {pointer + 4, change(memory, pointer + 3, value), io}

      # equals
      8 ->
        a = reference(memory, pointer, modes, 1)
        b = reference(memory, pointer, modes, 2)

        value =
          if a == b do
            1
          else
            0
          end

        {pointer + 4, change(memory, pointer + 3, value), io}

      # halt
      99 ->
        {length(memory), memory, io}

      unknown ->
        raise "Invalid instruction #{unknown}"
    end
  end

  defp reference(memory, pointer, modes, n) do
    if Enum.at(modes, n - 1, 0) == 1 do
      Enum.at(memory, pointer + n)
    else
      Enum.at(memory, Enum.at(memory, pointer + n))
    end
  end

  defp change(memory, pointer, value) do
    List.replace_at(memory, Enum.at(memory, pointer), value)
  end
end
