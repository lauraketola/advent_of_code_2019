defmodule Day6 do
  @moduledoc "Universal Orbit Map"

  def solution_part1(filename) do
    loadinput(filename)
    |> parse_orbits
    |> start_walk
    |> List.flatten()
    |> Enum.sum()
  end

  def solution_part2(filename) do
    loadinput(filename)
    |> find_santa()
  end

  defp loadinput(filename) do
    File.read!(filename)
    |> String.split("\n")
    |> Enum.map(&String.split(&1, "\)"))
  end

  # Part 1

  # Map objects to their orbiters
  defp parse_orbits(orbit_list) do
    map = %{}

    Enum.reduce(orbit_list, map, fn orbit, map_acc ->
      insert_orbit_to_map(map_acc, orbit)
    end)
  end

  defp insert_orbit_to_map(map, orbit) do
    object = Enum.at(orbit, 0)
    orbiter = Enum.at(orbit, 1)
    # Add orbiter to the list of object's orbiters,
    # and create new list if it's the first
    Map.put(map, object, [orbiter | Map.get(map, object, [])])
  end

  defp start_walk(map) do
    walk_and_count(map["COM"], map, 0)
  end

  defp walk_and_count(orbiters, _, _depth)
       when orbiters == [] do
    []
  end

  # For every orbiting object, mark its depth, and recurse deeper
  defp walk_and_count(orbiters, map, depth) do
    Enum.map(orbiters, fn orbiter ->
      orbiters_of_orbiter = Map.get(map, orbiter, [])
      depth = depth + 1

      [depth, walk_and_count(orbiters_of_orbiter, map, depth)]
    end)
  end

  # Part 2

  # Map every orbiting object to its parent
  defp parse_reverse_orbits(orbit_list) do
    map = %{}

    Enum.reduce(orbit_list, map, fn orbit, acc ->
      key = Enum.at(orbit, 1)
      value = Enum.at(orbit, 0)
      Map.put(acc, key, value)
    end)
  end

  defp find_santa(list) do
    map_rise = parse_reverse_orbits(list)

    # For both objects, find the path to COM.
    santa = path_upwards(map_rise, "SAN", [])
    you = path_upwards(map_rise, "YOU", [])

    weave_paths(santa, you)
  end

  defp path_upwards(_, current, path)
       when current == nil do
    path
  end

  defp path_upwards(map_rise, current, path) do
    com = Map.get(map_rise, current, nil)
    path_upwards(map_rise, com, [current | path])
  end

  defp weave_paths(santa, you) do
    # Drop the parts that are common to both,
    # to find the unique parts
    if hd(santa) == hd(you) do
      weave_paths(tl(santa), tl(you))
    else
      # Finally, sum the lengths of the unique parts
      length(you) + length(santa) - 2
    end
  end
end
