defmodule Day1Test do
  use ExUnit.Case, async: true

  test "day 1, part 1" do
    result_part1 = Day1.solution_part1("test/data/day1")
    assert result_part1 == 3_188_480
  end

  test "day 1, part 2" do
    result_part2 = Day1.solution_part2("test/data/day1")
    assert result_part2 == 4_779_847
  end
end
