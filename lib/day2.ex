defmodule Day2 do
  @moduledoc "1202 Program Alarm"

  def solution_part1(filename) do
    input = Intcode.loadinput(filename)
    Intcode.run(input, 12, 2)
  end

  def solution_part2(filename) do
    input = Intcode.loadinput(filename)
    host = self()
    realm = for x <- 0..99, y <- 0..99, do: {x, y}

    Enum.each(realm, fn {x, y} ->
      spawn(fn ->
        if Intcode.run(input, x, y) == 19_690_720 do
          send(host, {:solution, 100 * x + y})
        end
      end)
    end)

    receive do
      {:solution, answer} -> answer
    end
  end
end
