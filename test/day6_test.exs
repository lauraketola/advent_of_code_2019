defmodule Day6Test do
  use ExUnit.Case, async: true

  test "day 6, part 1" do
    assert Day6.solution_part1("test/data/day6_small") == 42
    assert Day6.solution_part1("test/data/day6") == 140_608
  end

  test "day 6, part 2" do
    assert Day6.solution_part2("test/data/day6_santa") == 4
    assert Day6.solution_part2("test/data/day6") == 337
  end
end
