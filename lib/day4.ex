defmodule Day4 do
  @moduledoc "Secure Container"

  def solution_part1(filename) do
    list = load_file(filename)
    Enum.count(list, &check_string/1)
  end

  def solution_part2(filename) do
    list = load_file(filename)
    Enum.count(list, &check_string_no_run/1)
  end

  defp load_file(filename) do
    [lower, upper] =
      File.read!(filename)
      |> String.trim()
      |> String.split("-")
      |> Enum.map(&String.to_integer/1)

    lower..upper
    |> Enum.map(&Integer.to_string/1)
    |> Enum.map(&String.pad_leading(&1, 6, "0"))
  end

  def parse_string(string) do
    string
    |> String.codepoints()
    |> Enum.map(&String.to_integer/1)
  end

  def check_string(string) do
    parse_string(string)
    |> check_code(nil, false)
  end

  def check_string_no_run(string) do
    parse_string(string)
    |> check_code_no_run(nil, nil, false)
  end

  # Part 1

  defp check_code(code, _, double)
       when code == [] do
    double
  end

  defp check_code(code, prev, double) do
    number = hd(code)

    if prev && number < prev do
      false
    else
      if number == prev do
        check_code(tl(code), number, true)
      else
        check_code(tl(code), number, double)
      end
    end
  end

  # Part 2

  defp check_code_no_run(code, _, _, double)
       when code == [] do
    double
  end

  defp check_code_no_run(code, prev, prevprev, double) do
    number = hd(code)

    if prev && number < prev do
      false
    else
      if number == prev and number != prevprev do
        if tl(code) != [] and number == hd(tl(code)) do
          check_code_no_run(tl(code), number, prev, double)
        else
          check_code_no_run(tl(code), number, prev, true)
        end
      else
        check_code_no_run(tl(code), number, prev, double)
      end
    end
  end
end
