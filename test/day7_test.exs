defmodule Day7Test do
  use ExUnit.Case, async: true

  test "day 7, part 1" do
    assert Day7.run(
             [3, 15, 3, 16, 1002, 16, 10, 16, 1, 16, 15, 15, 4, 15, 99, 0, 0],
             [4, 3, 2, 1, 0]
           ) == 43210

    assert Day7.solution_part1("test/data/day7") == 366_376
  end

  test "day 7, part 2" do
    assert Day7.solution_part2("test/data/day7") == 21_596_786
  end
end
