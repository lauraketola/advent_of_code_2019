defmodule Day3Test do
  use ExUnit.Case, async: true

  test "day 3, part 1" do
    assert Day3.solution_part1("test/data/day3") == 293
  end

  test "day 3, part 2" do
    assert Day3.solution_part2("test/data/day3") == 27306
  end
end
