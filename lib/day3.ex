defmodule Day3 do
  @moduledoc "Crossed Wires"

  def solution_part1(filename) do
    get_wire_intersections(filename)
    |> Enum.map(fn {a, _} -> a end)
    |> Enum.map(&manhattan/1)
    |> Enum.min()
  end

  def solution_part2(filename) do
    get_wire_intersections(filename)
    |> Enum.map(&combined_steps/1)
    |> Enum.min()
  end

  defp get_wire_intersections(filename) do
    wire1 =
      parse_input(filename, 0)
      |> navigate()

    wire2 =
      parse_input(filename, 1)
      |> navigate()

    intersection_recurser(wire1, wire2, []) |> tl
  end

  defp navigate(route) do
    Enum.reduce(route, [{0, 0, 0}], fn command, acc_route ->
      move(acc_route, command)
    end)
    |> List.keysort(1)
    |> List.keysort(0)
  end

  defp move(route, {direction, movement}) do
    case direction do
      "U" ->
        step(route, movement, 0, 1)

      "D" ->
        step(route, movement, 0, -1)

      "L" ->
        step(route, movement, -1, 0)

      "R" ->
        step(route, movement, 1, 0)
    end
  end

  defp parse_input(filename, n) do
    File.read!(filename)
    |> String.trim()
    |> String.split("\n")
    |> Enum.at(n)
    |> String.split(",")
    |> Enum.map(
      &{String.first(&1),
       String.slice(&1, 1..-1)
       |> String.to_integer()}
    )
  end

  defp intersection_recurser(wire1, wire2, result)
       when wire1 == [] or wire2 == [] do
    result
  end

  defp intersection_recurser(wire1, wire2, result) do
    case compare_duple(hd(wire1), hd(wire2)) do
      :equal ->
        intersection_recurser(
          tl(wire1),
          tl(wire2),
          [{hd(wire1), hd(wire2)} | result]
        )

      :smaller ->
        intersection_recurser(tl(wire1), wire2, result)

      :greater ->
        intersection_recurser(wire1, tl(wire2), result)
    end
  end

  defp compare_duple({x1, x2, _}, {y1, y2, _}) do
    if x1 == y1 do
      if x2 == y2 do
        :equal
      else
        if x2 < y2 do
          :smaller
        else
          :greater
        end
      end
    else
      if x1 < y1 do
        :smaller
      else
        :greater
      end
    end
  end

  defp step(route, steps, x, y) do
    Enum.reduce(1..steps, route, fn _, route_new ->
      {last_x, last_y, count} = List.first(route_new)
      {new_x, new_y} = {last_x + x, last_y + y}
      [{new_x, new_y, count + 1} | route_new]
    end)
  end

  defp manhattan({x, y, _}) do
    abs(x) + abs(y)
  end

  defp combined_steps({{_, _, steps1}, {_, _, steps2}}) do
    steps1 + steps2
  end
end
